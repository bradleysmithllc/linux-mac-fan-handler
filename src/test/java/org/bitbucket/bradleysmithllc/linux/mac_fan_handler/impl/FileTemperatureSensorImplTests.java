package org.bitbucket.bradleysmithllc.linux.mac_fan_handler.impl;

import org.bitbucket.bradleysmithllc.linux.mac_fan_handler.TemperatureSensor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class FileTemperatureSensorImplTests {
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	public File baseDir = null;

	@Before
	public void createSensorRoot() throws IOException {
		initializeSensorRoot("initial_root");
	}

	private void initializeSensorRoot(String tag) throws IOException {
		baseDir = temporaryFolder.newFolder(tag);
		baseDir.mkdirs();
	}

	@Test
	public void labelName() {
		Assert.assertEquals("temp1_label", FileTemperatureSensorImpl.generateTempLabelFileName(1));
		Assert.assertEquals("temp17_label", FileTemperatureSensorImpl.generateTempLabelFileName(17));
	}

	@Test
	public void inputName() {
		Assert.assertEquals("temp1_input", FileTemperatureSensorImpl.generateTempInputFileName(1));
		Assert.assertEquals("temp17_input", FileTemperatureSensorImpl.generateTempInputFileName(17));
	}

	@Test
	public void label() throws IOException {
		// create label file and let the fan control read it.
		createTempSensorFiles(1, "Fan 1 Label", 125);
		TemperatureSensor fc = new FileTemperatureSensorImpl(baseDir, 1);

		Assert.assertEquals("Fan 1 Label", fc.label());
	}

	@Test
	public void currentTemperature() throws IOException {
		// create label file and let the fan control read it.
		createTempSensorFiles(1, "Fan 1 Label", 125);
		TemperatureSensor fc = new FileTemperatureSensorImpl(baseDir, 1);

		Assert.assertEquals(125, fc.currentTemperature());
	}

	@Test
	public void scanSensors() {
		Assert.assertEquals(0, FileTemperatureSensorImpl.scanTemperatureSensors(baseDir).size());
	}

	@Test
	public void scanSensorOneSensor() throws IOException {
		createTempSensorFiles(1, "Se1", 0);
		Assert.assertEquals(1, FileTemperatureSensorImpl.scanTemperatureSensors(baseDir).size());
	}

	@Test
	public void scanSensorTenSensors() throws IOException {
		createTempSensorFiles(1, "Se1");
		createTempSensorFiles(2, "Se2");
		createTempSensorFiles(3, "Se3");
		createTempSensorFiles(4, "Se4");
		createTempSensorFiles(5, "Se5");
		createTempSensorFiles(6, "Se6");
		createTempSensorFiles(7, "Se7");
		createTempSensorFiles(8, "Se8");
		createTempSensorFiles(9, "Se9");
		createTempSensorFiles(10, "Se10");

		List<TemperatureSensor> temperatureSensorMap = FileTemperatureSensorImpl.scanTemperatureSensors(baseDir);
		Assert.assertEquals(10, temperatureSensorMap.size());

		checkSensor(temperatureSensorMap, "Se1");
		checkSensor(temperatureSensorMap, "Se2");
		checkSensor(temperatureSensorMap, "Se3");
		checkSensor(temperatureSensorMap, "Se4");
		checkSensor(temperatureSensorMap, "Se5");
		checkSensor(temperatureSensorMap, "Se6");
		checkSensor(temperatureSensorMap, "Se7");
		checkSensor(temperatureSensorMap, "Se8");
		checkSensor(temperatureSensorMap, "Se9");
		checkSensor(temperatureSensorMap, "Se10");
	}

	private void checkSensor(List<TemperatureSensor> temperatureSensorMap, String sensorLabel) {
		for (TemperatureSensor sensor : temperatureSensorMap) {
			if (sensor.label().equals(sensorLabel)) {
				return;
			}
		}

		Assert.fail();
	}

	private void createTempSensorFiles(int fanNumber, String labelText) throws IOException {
		createTempSensorFiles(fanNumber, labelText, 0);
	}

	private void createTempSensorFiles(int fanNumber, String labelText, int currentTemperature) throws IOException {
		FileFanControlImplTests.writeToFile(new File(baseDir, FileTemperatureSensorImpl.generateTempLabelFileName(fanNumber)), labelText);
		FileFanControlImplTests.writeToFile(new File(baseDir, FileTemperatureSensorImpl.generateTempInputFileName(fanNumber)), String.valueOf(currentTemperature));
	}
}
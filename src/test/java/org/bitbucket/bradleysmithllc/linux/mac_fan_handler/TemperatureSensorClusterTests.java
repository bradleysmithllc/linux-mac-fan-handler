package org.bitbucket.bradleysmithllc.linux.mac_fan_handler;

import org.junit.Assert;
import org.junit.Test;

public class TemperatureSensorClusterTests {
	@Test
	public void noSamples() {
		Assert.assertEquals(0, TemperatureSensorCluster.calculateNewFanSpeed(0, 0));
	}

	@Test
	public void oneSample() {
		Assert.assertEquals(550, TemperatureSensorCluster.calculateNewFanSpeed(0, 1000, 70000.0d));
	}

	@Test
	public void minimum() {
		Assert.assertEquals(7, TemperatureSensorCluster.calculateNewFanSpeed(7, 20, 00000.0d));
	}

	@Test
	public void maximum() {
		Assert.assertEquals(20, TemperatureSensorCluster.calculateNewFanSpeed(7, 20, 9990000.0d));
	}

	@Test
	public void middle() {
		Assert.assertEquals(1050, TemperatureSensorCluster.calculateNewFanSpeed(0, 2000, 60000D, 70000D, 80000D));
	}

	@Test
	public void sample100() {
		Assert.assertEquals(1100, TemperatureSensorCluster.calculateNewFanSpeed(1100, 5500, 30000.0, 30812.5, 84000.0));
	}

	@Test
	public void sample75() {
		Assert.assertEquals(3350, TemperatureSensorCluster.calculateNewFanSpeed(1100, 5500, 42000.0, 84812.5, 84000.0));
	}

	@Test
	public void sample50() {
		Assert.assertEquals(4900, TemperatureSensorCluster.calculateNewFanSpeed(1100, 5500, 84000.0, 84812.5, 84000.0));
	}

	@Test
	public void sampleHigh() {
		Assert.assertEquals(5500, TemperatureSensorCluster.calculateNewFanSpeed(1100, 5500, 90000.0));
	}
}
package org.bitbucket.bradleysmithllc.linux.mac_fan_handler.impl;

import org.bitbucket.bradleysmithllc.linux.mac_fan_handler.FanControl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class FileFanControlImplTests {
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	public File baseDir = null;

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Before
	public void createFanRoot() throws IOException {
		initializeFanRoot("initial_root");
	}

	private void initializeFanRoot(String tag) throws IOException {
		baseDir = temporaryFolder.newFolder(tag);
		baseDir.mkdirs();
	}

	@Test
	public void labelName() {
		Assert.assertEquals("fan1_label", FileFanControlImpl.generateFanLabelFileName(1));
		Assert.assertEquals("fan17_label", FileFanControlImpl.generateFanLabelFileName(17));
	}

	@Test
	public void minName() {
		Assert.assertEquals("fan1_min", FileFanControlImpl.generateFanMinimumFileName(1));
		Assert.assertEquals("fan17_min", FileFanControlImpl.generateFanMinimumFileName(17));
	}

	@Test
	public void maxName() {
		Assert.assertEquals("fan1_max", FileFanControlImpl.generateFanMaximumFileName(1));
		Assert.assertEquals("fan17_max", FileFanControlImpl.generateFanMaximumFileName(17));
	}

	@Test
	public void outputName() {
		Assert.assertEquals("fan1_output", FileFanControlImpl.generateFanOutputFileName(1));
		Assert.assertEquals("fan17_output", FileFanControlImpl.generateFanOutputFileName(17));
	}

	@Test
	public void inputName() {
		Assert.assertEquals("fan1_input", FileFanControlImpl.generateFanInputFileName(1));
		Assert.assertEquals("fan17_input", FileFanControlImpl.generateFanInputFileName(17));
	}

	@Test
	public void manualName() {
		Assert.assertEquals("fan1_manual", FileFanControlImpl.generateFanManualFileName(1));
		Assert.assertEquals("fan17_manual", FileFanControlImpl.generateFanManualFileName(17));
	}

	@Test
	public void label() throws IOException {
		// create label file and let the fan control read it.
		createFanFiles(1, "Fan 1 Label  ", 125, 0, 1722);
		FanControl fc = new FileFanControlImpl(baseDir, 1);

		Assert.assertEquals("Fan 1 Label", fc.label());
	}

	@Test
	public void minSpeed0() throws IOException {
		createFanFiles(1, "Fan 1 Label", 125, 0, 1722);
		Assert.assertEquals(0, new FileFanControlImpl(baseDir, 1).minSpeed());
	}

	@Test
	public void minSpeed1() throws IOException {
		createFanFiles(1, "Fan 1 Label", 125, 1, 1722);
		Assert.assertEquals(1, new FileFanControlImpl(baseDir, 1).minSpeed());
	}

	@Test
	public void minSpeed10000() throws IOException {
		createFanFiles(1, "Fan 1 Label", 125, 10000, 1722);
		Assert.assertEquals(10000, new FileFanControlImpl(baseDir, 1).minSpeed());
	}

	@Test
	public void maxSpeed0() throws IOException {
		createFanFiles(1, "Fan 1 Label", 125, 0, 0);
		Assert.assertEquals(0, new FileFanControlImpl(baseDir, 1).maxSpeed());
	}

	@Test
	public void maxSpeed1() throws IOException {
		createFanFiles(1, "Fan 1 Label", 125, 1, 1);
		Assert.assertEquals(1, new FileFanControlImpl(baseDir, 1).maxSpeed());
	}

	@Test
	public void maxSpeed10000() throws IOException {
		createFanFiles(1, "Fan 1 Label", 125, 10000, 10000);
		Assert.assertEquals(10000, new FileFanControlImpl(baseDir, 1).maxSpeed());
	}

	@Test
	public void currentSpeed0() throws IOException {
		createFanFiles(1, "Fan 1 Label", 0, 0, 0);
		Assert.assertEquals(0, new FileFanControlImpl(baseDir, 1).currentInputSpeed());
	}

	@Test
	public void currentSpeed1() throws IOException {
		createFanFiles(1, "Fan 1 Label", 1, 0, 0);
		Assert.assertEquals(1, new FileFanControlImpl(baseDir, 1).currentInputSpeed());
	}

	@Test
	public void currentSpeed10000() throws IOException {
		createFanFiles(1, "Fan 1 Label", 10000, 0, 0);
		Assert.assertEquals(10000, new FileFanControlImpl(baseDir, 1).currentInputSpeed());
	}

	@Test
	public void currentSpeedUpdates() throws IOException {
		createFanFiles(1, "Fan 1 Label", 0, 0, 0);
		FanControl fanControl = new FileFanControlImpl(baseDir, 1);

		createFanFiles(1, "Fan 1 Label", 1, 0, 0);
		Assert.assertEquals(1, fanControl.currentInputSpeed());
		createFanFiles(1, "Fan 1 Label", 10000, 0, 0);
		Assert.assertEquals(10000, fanControl.currentInputSpeed());
	}

	@Test
	public void setSpeedMiddle() throws IOException {
		createFanFiles(1, "Fan 1 Label", 0, 0, 0);
		FanControl fanControl = new FileFanControlImpl(baseDir, 1);

		fanControl.setOutputSpeed(0);
	}

	@Test
	public void setSpeedTooHigh() throws IOException {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Speed range out of bounds");
		createFanFiles(1, "Fan 1 Label", 0, 10, 11);
		FanControl fanControl = new FileFanControlImpl(baseDir, 1);

		fanControl.setOutputSpeed(12);
	}

	@Test
	public void setSpeedTooLow() throws IOException {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Speed range out of bounds");
		createFanFiles(1, "Fan 1 Label", 0, 10, 11);
		FanControl fanControl = new FileFanControlImpl(baseDir, 1);

		fanControl.setOutputSpeed(9);
	}

	@Test
	public void setSpeedMax() throws IOException {
		createFanFiles(1, "Fan 1 Label", 0, 10, 11);
		FanControl fanControl = new FileFanControlImpl(baseDir, 1);

		fanControl.setOutputSpeed(11);
	}

	@Test
	public void setSpeedMin() throws IOException {
		createFanFiles(1, "Fan 1 Label", 0, 10, 11);
		FanControl fanControl = new FileFanControlImpl(baseDir, 1);

		fanControl.setOutputSpeed(10);
	}

	@Test
	public void setSpeedCurrentSpeed() throws IOException {
		createFanFiles(1, "Fan 1 Label", 0, 10, 11);
		FanControl fanControl = new FileFanControlImpl(baseDir, 1);

		fanControl.setOutputSpeed(10);

		Assert.assertEquals(10, fanControl.currentOutputSpeed());
	}

	@Test
	public void fanMode() throws IOException {
		createFanFiles(1, "Fan 1 Label", 0, 0, 0, true);
		FanControl fanControl = new FileFanControlImpl(baseDir, 1);

		Assert.assertEquals(FanControl.fan_mode.manual, fanControl.currentFanMode());
		fanControl.setFanMode(FanControl.fan_mode.automatic);
		Assert.assertEquals(FanControl.fan_mode.automatic, fanControl.currentFanMode());
		fanControl.setFanMode(FanControl.fan_mode.manual);
		Assert.assertEquals(FanControl.fan_mode.manual, fanControl.currentFanMode());
	}

	@Test
	public void manyFans() throws IOException {
		createFanFiles(1, "Fan 1 Label", 1, 10, 100);
		createFanFiles(2, "Fan 2 Label", 2, 20, 200);

		FanControl fanControl1 = new FileFanControlImpl(baseDir, 1);
		FanControl fanControl2 = new FileFanControlImpl(baseDir, 2);

		Assert.assertEquals("Fan 1 Label", fanControl1.label());
		Assert.assertEquals(1, fanControl1.currentInputSpeed());
		Assert.assertEquals(10, fanControl1.minSpeed());
		Assert.assertEquals(100, fanControl1.maxSpeed());

		Assert.assertEquals("Fan 2 Label", fanControl2.label());
		Assert.assertEquals(2, fanControl2.currentInputSpeed());
		Assert.assertEquals(20, fanControl2.minSpeed());
		Assert.assertEquals(200, fanControl2.maxSpeed());
	}

	@Test
	public void scanFansNoFans() {
		Assert.assertEquals(0, FileFanControlImpl.scanFans(baseDir).size());
	}

	@Test
	public void scanFansOneFan() throws IOException {
		createFanFiles(1, "Fan1");
		Map<String, FanControl> fanControlMap = FileFanControlImpl.scanFans(baseDir);

		Assert.assertEquals(1, fanControlMap.size());
		Assert.assertTrue(fanControlMap.containsKey("fan1"));
	}

	@Test
	public void scanFansThreeFans() throws IOException {
		createFanFiles(1, "Fan1");
		createFanFiles(2, "Fan2");
		createFanFiles(3, "Fan3");

		Map<String, FanControl> fanControlMap = FileFanControlImpl.scanFans(baseDir);
		Assert.assertEquals(3, fanControlMap.size());

		Assert.assertTrue(fanControlMap.containsKey("fan1"));
		Assert.assertTrue(fanControlMap.containsKey("fan2"));
		Assert.assertTrue(fanControlMap.containsKey("fan3"));
	}

	private void createFanLabel(int fanNumber, String labelText) throws IOException {
		writeToFile(new File(baseDir, FileFanControlImpl.generateFanLabelFileName(fanNumber)), labelText);
	}

	private void createFanFiles(int fanNumber, String labelText) throws IOException {
		createFanFiles(fanNumber, labelText, 0, 0, 0, false);
	}

	private void createFanFiles(int fanNumber, String labelText, int currentOutput, int fanMinimum, int fanMaximum) throws IOException {
		createFanFiles(fanNumber, labelText, currentOutput, fanMinimum, fanMaximum, false);
	}

	private void createFanFiles(int fanNumber, String labelText, int currentOutput, int fanMinimum, int fanMaximum, boolean fanManual) throws IOException {
		createFanLabel(fanNumber, labelText);
		writeToFile(new File(baseDir, FileFanControlImpl.generateFanManualFileName(fanNumber)), fanManual ? "1" : "0");
		writeToFile(new File(baseDir, FileFanControlImpl.generateFanMinimumFileName(fanNumber)), String.valueOf(fanMinimum));
		writeToFile(new File(baseDir, FileFanControlImpl.generateFanMaximumFileName(fanNumber)), String.valueOf(fanMaximum));
		writeToFile(new File(baseDir, FileFanControlImpl.generateFanInputFileName(fanNumber)), String.valueOf(currentOutput));
	}

	public static void writeToFile(File file, String text) throws IOException {
		FileWriter fw = new FileWriter(file);
		fw.write(text);
		fw.close();
	}
}
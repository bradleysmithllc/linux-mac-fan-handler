package org.bitbucket.bradleysmithllc.linux.mac_fan_handler;

import java.io.File;
import java.io.IOException;

public interface TemperatureSensor {
	String label();
	int currentTemperature() throws IOException;

	int sensorNumber();

	File labelFile();
}
package org.bitbucket.bradleysmithllc.linux.mac_fan_handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class TemperatureSensorClusterArray {
	private final TemperatureSensorCluster oddSensorCluster;
	private final TemperatureSensorCluster hddSensorCluster;
	private final TemperatureSensorCluster cpuSensorCluster;
	private final TemperatureSensorCluster otherSensorCluster;

	public TemperatureSensorClusterArray(Map<String, FanControl> fanMap) {
		// create our sensor clusters
		oddSensorCluster = new TemperatureSensorCluster(TemperatureSensorCluster.sensor_family.odd, TemperatureSensorCluster.fanForSensorCluster(fanMap, TemperatureSensorCluster.sensor_family.odd));
		hddSensorCluster = new TemperatureSensorCluster(TemperatureSensorCluster.sensor_family.hdd, TemperatureSensorCluster.fanForSensorCluster(fanMap, TemperatureSensorCluster.sensor_family.hdd));
		cpuSensorCluster = new TemperatureSensorCluster(TemperatureSensorCluster.sensor_family.cpu, TemperatureSensorCluster.fanForSensorCluster(fanMap, TemperatureSensorCluster.sensor_family.cpu));
		otherSensorCluster = new TemperatureSensorCluster(TemperatureSensorCluster.sensor_family.other, TemperatureSensorCluster.fanForSensorCluster(fanMap, TemperatureSensorCluster.sensor_family.other));
	}

	public void visitArray(Consumer<TemperatureSensorCluster> consumer) {
		consumer.accept(oddSensorCluster);
		consumer.accept(hddSensorCluster);
		consumer.accept(cpuSensorCluster);
		consumer.accept(otherSensorCluster);
	}

	public void classifyTemperatureSensors(List<TemperatureSensor> sensorArray) {
		// for each sensor, classify and add to exactly one cluster
		for (TemperatureSensor sensorEntry : sensorArray) {
			TemperatureSensorCluster.sensor_family sensorFamily = TemperatureSensorCluster.classifyClusterLabel(sensorEntry);

			switch (sensorFamily) {
				case odd:
					oddSensorCluster.addTemperatureSensor(sensorEntry);
					break;
				case hdd:
					hddSensorCluster.addTemperatureSensor(sensorEntry);
					break;
				case cpu:
					cpuSensorCluster.addTemperatureSensor(sensorEntry);
					break;
				case other:
					hddSensorCluster.addTemperatureSensor(sensorEntry);
					break;
			}
		}
	}

	public void scanTemperatures() throws IOException {
		oddSensorCluster.adjustFan();
		hddSensorCluster.adjustFan();
		cpuSensorCluster.adjustFan();
		//otherSensorCluster.adjustFan();
	}
}
package org.bitbucket.bradleysmithllc.linux.mac_fan_handler;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LightLogFacade {
	private int sectionCount = 0;
	private int indent = 0;

	public void nextSection() {
		if (sectionCount > 0) {
			System.out.println();
		}

		sectionCount++;
	}

	public void logLine(String line) {
		System.out.print(new SimpleDateFormat("yyyyMMdd-HH:mm:ss").format(new Date()) + " ");
		indent();
		System.out.println(line);
	}

	private void indent() {
		System.out.print("\t\t\t\t\t\t\t\t".substring(0, indent));
	}

	public void decreaseIndent() {
		indent--;
	}

	public void increaseIndent() {
		indent++;
	}
}
package org.bitbucket.bradleysmithllc.linux.mac_fan_handler.impl;

import org.bitbucket.bradleysmithllc.linux.mac_fan_handler.TemperatureSensor;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileTemperatureSensorImpl implements TemperatureSensor {
	private final int sensorNumber;
	private final String label;
	private final File baseDir;

	public FileTemperatureSensorImpl(File baseDir, int sensorNumber) throws IOException {
		this.sensorNumber = sensorNumber;
		this.baseDir = baseDir;

		label = FileFanControlImpl.readFileText(new File(baseDir, generateTempLabelFileName(sensorNumber)));
	}

	@Override
	public int sensorNumber() {
		return sensorNumber;
	}

	@Override
	public File labelFile() {
		return new File(baseDir, generateTempLabelFileName(sensorNumber));
	}

	@Override
	public String label() {
		return label;
	}

	@Override
	public int currentTemperature() throws IOException {
		String text = FileFanControlImpl.readFileText(new File(baseDir, generateTempInputFileName(sensorNumber)));
		return Integer.parseInt(text);
	}

	public static String generateTempLabelFileName(int sensorNumber) {
		// name pattern is <baseDir>/fan{fanNumber}_label
		return "temp" + sensorNumber + "_label";
	}

	public static String generateTempInputFileName(int sensorNumber) {
		// name pattern is <baseDir>/fan{fanNumber}_label
		return "temp" + sensorNumber + "_input";
	}

	public static List<TemperatureSensor> scanTemperatureSensors(final File... baseDir) {
		final List<TemperatureSensor> sensorArray = new ArrayList<>();

		for (File dir : baseDir) {
			scanTemperatureSensors(sensorArray, dir);
		}

		return sensorArray;
	}

	private static void scanTemperatureSensors(final List<TemperatureSensor> sensorArray, final File baseDir) {
		final List<TemperatureSensor> tempArray = new ArrayList<>();

		final Pattern fanLabelPattern = Pattern.compile("temp(\\d+)_label");

		baseDir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				Matcher matcher = fanLabelPattern.matcher(pathname.getName());

				if (matcher.matches()) {
					int fanNumber = Integer.parseInt(matcher.group(1));

					try {
						TemperatureSensor ts = new FileTemperatureSensorImpl(baseDir, fanNumber);
						tempArray.add(ts);
					} catch (IOException e) {
						e.printStackTrace(System.out);
					}
				}

				return false;
			}
		});

		Collections.sort(tempArray, (sensor1, sensor2) -> {
			if (sensor1.sensorNumber() < sensor2.sensorNumber()) {
				return -1;
			} else if (sensor1.sensorNumber() == sensor2.sensorNumber()) {
				return 0;
			} else {
				return 1;
			}
		});

		sensorArray.addAll(tempArray);
	}

	@Override
	public String toString() {
		return "FileTemperatureSensorImpl{" +
				"sensorNumber=" + sensorNumber +
				", label='" + label + '\'' +
				'}';
	}
}
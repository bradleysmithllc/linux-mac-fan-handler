package org.bitbucket.bradleysmithllc.linux.mac_fan_handler;

import org.bitbucket.bradleysmithllc.linux.mac_fan_handler.impl.FileFanControlImpl;
import org.bitbucket.bradleysmithllc.linux.mac_fan_handler.impl.FileTemperatureSensorImpl;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class MacFanHandlerService {
	public static String BASE_SYS = "/sys/class/hwmon";
	public static String BASE_MVN = "src/main" + BASE_SYS;

	public static String USE_BASE = BASE_SYS;

	public static final LightLogFacade logFacade = new LightLogFacade();

	public static void main(String... argv) throws Exception {
		logFacade.logLine("Scanning for fans...");
		Map<String, FanControl> fanMap = FileFanControlImpl.scanFans(new File(USE_BASE + "/hwmon1/device"));
		logFacade.logLine("Done.\n");

		logFacade.nextSection();
		logFans(fanMap);

		TemperatureSensorClusterArray temperatureSensorClusterArray = new TemperatureSensorClusterArray(fanMap);

		logFacade.nextSection();
		logFacade.logLine("Scanning for temperature sensors...");
		List<TemperatureSensor> sensorArray = FileTemperatureSensorImpl.scanTemperatureSensors(
				new File(USE_BASE + "/hwmon1/device"),
				new File(USE_BASE + "/hwmon0/")
		);
		logFacade.logLine("Done.");

		logFacade.nextSection();
		logTemperatureSensors(sensorArray);

		logFacade.nextSection();
		logFacade.logLine("Classifying temperature sensors...");
		logFacade.increaseIndent();
		temperatureSensorClusterArray.classifyTemperatureSensors(sensorArray);
		temperatureSensorClusterArray.visitArray((cluster) -> {
			logFacade.logLine(cluster.sensorFamily().name() + " has " + cluster.sensorList().size() + " sensors");
		});
		logFacade.decreaseIndent();
		logFacade.logLine("Done.");

		AtomicInteger sensorCount = new AtomicInteger(0);

		logFacade.nextSection();
		temperatureSensorClusterArray.visitArray((cluster) -> {
			sensorCount.addAndGet(cluster.sensorList().size());
		});
		logFacade.logLine(sensorCount.get() + " sensors classified.");

		logFacade.nextSection();
		logFacade.logLine("Setting fans to manual control...");
		logFacade.increaseIndent();
		for (FanControl fanControl : fanMap.values()) {
			fanControl.setFanMode(FanControl.fan_mode.manual);
			logFacade.logLine("set fan " + fanControl.fanNumber() + " to manual");
		}
		logFacade.decreaseIndent();
		logFacade.logLine("Done.");

		// main loop
		while (true) {
			logFacade.nextSection();
			temperatureSensorClusterArray.scanTemperatures();

			// wait to process again.  Process in 10 second intervals in case the computer heats up rapidly
			Thread.sleep(10000);
		}
	}

	private static void logTemperatureSensors(List<TemperatureSensor> sensorArray) throws IOException {
		logFacade.logLine("Discovered " + sensorArray.size() + " temperature sensors...");

		logFacade.logLine("====================================");

		boolean first = true;
		for (TemperatureSensor temperatureSensor : sensorArray) {
			if (!first) {
				System.out.println();
			}

			if (first) {
				first = false;
			}

			logFacade.logLine("Sensor number " + temperatureSensor.sensorNumber());
			logFacade.increaseIndent();
			logFacade.logLine("path: " + temperatureSensor.labelFile().getAbsolutePath());
			logFacade.logLine("label: " + temperatureSensor.label());
			logFacade.logLine("currentTemperature: " + temperatureSensor.currentTemperature());
			logFacade.decreaseIndent();
		}

		logFacade.logLine("====================================");
	}

	private static void logFans(Map<String, FanControl> fanMap) throws IOException {
		List<FanControl> fanList = new ArrayList<>(fanMap.values());

		// put them in numerical order so the output is repeatable.  This is why we cloned the list.
		Collections.sort(fanList, (fanControl1, fanControl2) -> {
			if (fanControl1.fanNumber() < fanControl2.fanNumber()) {
				return -1;
			}
			else if (fanControl1.fanNumber() == fanControl2.fanNumber()) {
				return 0;
			}
			else {
				return 1;
			}
		});

		logFacade.logLine("Discovered " + fanList.size() + " fans...");

		logFacade.logLine("====================================");

		boolean first = true;
		for (FanControl fanControl : fanList) {
			if (!first) {
				System.out.println();
			}

			if (first) {
				first = false;
			}

			logFacade.logLine("Fan number " + fanControl.fanNumber());
			logFacade.increaseIndent();
			logFacade.logLine("path: " + fanControl.labelFile().getAbsolutePath());
			logFacade.logLine("label: " + fanControl.label());
			logFacade.logLine("minSpeed: " + fanControl.minSpeed());
			logFacade.logLine("maxSpeed: " + fanControl.maxSpeed());
			logFacade.logLine("currentSpeed: " + fanControl.currentInputSpeed());
			logFacade.logLine("currentMode: " + fanControl.currentFanMode());
			logFacade.decreaseIndent();
		}
		logFacade.logLine("====================================");
	}
}
package org.bitbucket.bradleysmithllc.linux.mac_fan_handler;

import java.io.File;
import java.io.IOException;

public interface FanControl {
	String label();

	int maxSpeed();

	int minSpeed();

	int currentInputSpeed() throws IOException;
	int currentOutputSpeed() throws IOException;

	fan_mode currentFanMode() throws IOException;

	void setFanMode(fan_mode mode) throws IOException;

	int fanNumber();

	File labelFile();

    void setOutputSpeed(int newFanSpeed) throws IOException;

    enum speed_type {
		min,
		max,
		input, // The actual, measured speed of the fan
		output // The requested, or target speed
	}

	public enum fan_mode {
		automatic,
		manual
	}
}

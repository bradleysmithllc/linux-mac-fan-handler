package org.bitbucket.bradleysmithllc.linux.mac_fan_handler;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TemperatureSensorCluster {
	private final List<TemperatureSensor> sensorArray = new ArrayList<>();
	private final sensor_family clusterType;
	private final FanControl fanControl;

	public List<TemperatureSensor> sensorList() {
		return sensorArray;
	}

	public enum sensor_family {
		odd,
		hdd,
		cpu,
		other
	}

	public TemperatureSensorCluster(sensor_family clusterLabel, FanControl fanControl) {
		this.clusterType = clusterLabel;
		this.fanControl = fanControl;
	}

	public void addTemperatureSensor(TemperatureSensor sensor) {
		sensorArray.add(sensor);
	}

	public sensor_family sensorFamily() {
		return clusterType;
	}

	public void adjustFan() throws IOException {
		double[] data = new double[sensorArray.size()];

		for (int index = 0; index < sensorArray.size(); index++) {
			data[index] = sensorArray.get(index).currentTemperature();
		}

		Percentile percentile = new Percentile();
		percentile.setData(data);

		StringBuilder stb = new StringBuilder(sensorFamily().name()).append(" | ")
			.append(percentile.evaluate(1.0d)).append(" | ")
			.append(percentile.evaluate(25.0d)).append(" | ")
			.append(percentile.evaluate(50.0d)).append(" | ")
			.append(percentile.evaluate(75.0d)).append(" | ")
			.append(percentile.evaluate(100.0d));

		// find the average of the top 3 temperatures
		int newFanSpeed = calculateNewFanSpeed(
			fanControl.minSpeed(),
			fanControl.maxSpeed(),
			percentile.evaluate(50.0d),
			percentile.evaluate(75.0d),
			percentile.evaluate(100.0d)
		);

		int fanSpeed = fanControl.currentOutputSpeed();
		String action = "none";
		// only decrease the fan speed if the change is to minimum, or at least 500 down
		if (newFanSpeed == fanControl.minSpeed() || (fanSpeed - newFanSpeed) > 200) {
			if (fanControl.currentOutputSpeed() != newFanSpeed) {
				fanControl.setOutputSpeed(newFanSpeed);
				action = "chan";
			}
		} else if (newFanSpeed == fanControl.maxSpeed()) {
			if (fanControl.currentOutputSpeed() != newFanSpeed) {
				fanControl.setOutputSpeed(newFanSpeed);
				action = "chan";
			}
		} else if (newFanSpeed > fanSpeed) {
			fanControl.setOutputSpeed(newFanSpeed);
			action = "chan";
		}

		stb.append(" | ").append(fanControl.fanNumber());
		stb.append(" | ").append(fanControl.label());
		stb.append(" | ").append(StringUtils.leftPad(String.valueOf(fanControl.minSpeed()), 4, " "));
		stb.append(" | ").append(StringUtils.leftPad(String.valueOf(fanControl.maxSpeed()), 4, " "));
		stb.append(" | ").append(StringUtils.leftPad(String.valueOf(fanSpeed), 4, " "));

		BigDecimal fsbd = new BigDecimal(fanSpeed, new MathContext(2, RoundingMode.HALF_EVEN)).setScale(2);
		BigDecimal fmbd = new BigDecimal(fanControl.maxSpeed(), new MathContext(2, RoundingMode.HALF_EVEN)).setScale(2);
		stb.append(" | ").append(StringUtils.leftPad(fsbd.divide(fmbd, RoundingMode.HALF_EVEN).movePointRight(2).toString(), 2, "0")).append("%");
		stb.append(" | ").append(StringUtils.leftPad(String.valueOf(newFanSpeed), 4, " "));
		stb.append(" | ").append(action);
		MacFanHandlerService.logFacade.logLine(stb.toString());
	}

	public static sensor_family classifyClusterLabel(TemperatureSensor sensor) {
		// classify in groups.  Anything Core... or TC... is a CPU.
		String toClassify = sensor.label().toLowerCase();

		if (
			toClassify.startsWith("core")
			||
			toClassify.startsWith("tc")
		) {
			return sensor_family.cpu;
		}
		else if (
			toClassify.startsWith("tg")
			||
			toClassify.startsWith("to")
		) {
			return sensor_family.odd;
		}
		else if (
			toClassify.startsWith("th")
			||
			toClassify.startsWith("tl")
			||
			toClassify.startsWith("tw")
			||
			toClassify.startsWith("tm")
			||
			toClassify.startsWith("tp")
		) {
			return sensor_family.hdd;
		}

		return sensor_family.other;
	}

	public static FanControl fanForSensorCluster(Map<String, FanControl> fanMap, TemperatureSensorCluster.sensor_family other) {
		switch(other) {
			case odd:
				if (fanMap.containsKey("odd")) {
					return fanMap.get("odd");
				}
				break;
			case hdd:
				if (fanMap.containsKey("hdd")) {
					return fanMap.get("hdd");
				}
				break;
			case cpu:
				if (fanMap.containsKey("cpu")) {
					return fanMap.get("cpu");
				}
				break;
		}

		// default to the highest-RPM fan
		FanControl maxFan = null;

		for (FanControl fanControl : fanMap.values()) {
			if (maxFan == null) {
				maxFan = fanControl;
			}

			if (maxFan.maxSpeed() < fanControl.maxSpeed()) {
				maxFan = fanControl;
			}
		}

		return maxFan;
	}

	public static int calculateNewFanSpeed(int minSpeed, int maxSpeed, double... samples) {
		double total = 0d;

		for (double sample : samples) {
			total += sample;
		}

		total = total / samples.length;

		// scale the fan as a percentage from 50 - 90 degrees
		total -= 50000.0d;
		total = total / 40000.0d;

		// suggested fan speed is this fraction times the fan delta
		double newFanSpeed = minSpeed + ((maxSpeed - minSpeed) * total);

		int newFanSpeedInt = (int) newFanSpeed;
		newFanSpeedInt += 50;
		newFanSpeedInt = newFanSpeedInt - newFanSpeedInt % 50;
		return Math.min(Math.max(newFanSpeedInt, minSpeed), maxSpeed);
	}
}
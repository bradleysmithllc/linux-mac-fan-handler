package org.bitbucket.bradleysmithllc.linux.mac_fan_handler.impl;

import org.bitbucket.bradleysmithllc.linux.mac_fan_handler.FanControl;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileFanControlImpl implements FanControl {
	private final int fanNumber;
	private final String label;
	private final int maxSpeed;
	private final int minSpeed;
	private final File baseDir;

	public FileFanControlImpl(File baseDir, int fanNumber) throws IOException {
		this.baseDir = baseDir;
		this.fanNumber = fanNumber;

		// grab the label from the fan control file
		label = readFanLabel(baseDir, fanNumber);
		maxSpeed = readFanMaxSpeed(baseDir, fanNumber);
		minSpeed = readFanMinSpeed(baseDir, fanNumber);
	}

	private static int readFanMinSpeed(File baseDir, int fanNumber) throws IOException {
		return readFanSpeed(baseDir, fanNumber, speed_type.min);
	}

	private static int readFanSpeed(File baseDir, int fanNumber, speed_type st) throws IOException {
		File speedFile;

		switch (st) {
			case min:
				speedFile = new File(baseDir, generateFanMinimumFileName(fanNumber));
				break;
			case max:
				speedFile = new File(baseDir, generateFanMaximumFileName(fanNumber));
				break;
			case input:
				speedFile = new File(baseDir, generateFanInputFileName(fanNumber));
				break;
			case output:
				speedFile = new File(baseDir, generateFanOutputFileName(fanNumber));
				break;
			default:
				throw new IllegalStateException();
		}

		String text = readFileText(speedFile);

		return Integer.parseInt(text);
	}

	private static int readFanMaxSpeed(File baseDir, int fanNumber) throws IOException {
		return readFanSpeed(baseDir, fanNumber, speed_type.max);
	}

	private static int readFanCurrentSpeed(File baseDir, int fanNumber) throws IOException {
		return readFanSpeed(baseDir, fanNumber, speed_type.input);
	}

	public static String generateFanLabelFileName(int fanNumber) {
		// name pattern is <baseDir>/fan{fanNumber}_label
		return "fan" + fanNumber + "_label";
	}

	public static String generateFanMinimumFileName(int fanNumber) {
		// name pattern is <baseDir>/fan{fanNumber}_min
		return "fan" + fanNumber + "_min";
	}

	public static String generateFanMaximumFileName(int fanNumber) {
		// name pattern is <baseDir>/fan{fanNumber}_max
		return "fan" + fanNumber + "_max";
	}

	public static String generateFanOutputFileName(int fanNumber) {
		// name pattern is <baseDir>/fan{fanNumber}_output
		return "fan" + fanNumber + "_output";
	}

	public static String generateFanInputFileName(int fanNumber) {
		// name pattern is <baseDir>/fan{fanNumber}_input
		return "fan" + fanNumber + "_input";
	}

	public static String generateFanManualFileName(int fanNumber) {
		// name pattern is <baseDir>/fan{fanNumber}_manual
		return "fan" + fanNumber + "_manual";
	}

	public static String readFanLabel(File baseDir, int fanNumber) throws IOException {
		// name pattern is <baseDir>/fan{fanNumber}_label
		File fanLabelFile = new File(baseDir, generateFanLabelFileName(fanNumber));

		return readFileText(fanLabelFile);
	}

	public static String readFileText(File fanFile) throws IOException {
		FileReader fileReader = new FileReader(fanFile);
		BufferedReader br = new BufferedReader(fileReader);

		try {
			return br.readLine().trim();
		} finally {
			br.close();
		}
	}

	public static void writeFileText(File fanFile, String text) throws IOException {
		FileWriter fileWriter = new FileWriter(fanFile);

		try {
			fileWriter.write(text);
		} finally {
			fileWriter.close();
		}
	}

	@Override
	public String label() {
		return label;
	}

	@Override
	public int maxSpeed() {
		return maxSpeed;
	}

	@Override
	public int minSpeed() {
		return minSpeed;
	}

	@Override
	public int currentInputSpeed() throws IOException {
		return readFanSpeed(baseDir, fanNumber, speed_type.input);
	}

	@Override
	public int currentOutputSpeed() throws IOException {
		return readFanSpeed(baseDir, fanNumber, speed_type.output);
	}

	@Override
	public fan_mode currentFanMode() throws IOException {
		String mode = readFileText(new File(baseDir, generateFanManualFileName(fanNumber)));

		if (mode.equals("0")) {
			return fan_mode.automatic;
		} else {
			return fan_mode.manual;
		}
	}

	@Override
	public void setFanMode(fan_mode mode) throws IOException {
		File target = new File(baseDir, generateFanManualFileName(fanNumber));

		String fileText = "";

		switch (mode) {
			case automatic:
				fileText = "0";
				break;
			case manual:
				fileText = "1";
				break;
		}

		writeFileText(target, fileText);
	}

	public static Map<String, FanControl> scanFans(final File baseDir) {
		final Map<String, FanControl> fanMap = new HashMap<>();
		final Pattern fanLabelPattern = Pattern.compile("fan(\\d)_label");

		baseDir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				Matcher matcher = fanLabelPattern.matcher(pathname.getName());

				if (matcher.matches()) {
					int fanNumber = Integer.parseInt(matcher.group(1));

					try {
						FanControl fc = new FileFanControlImpl(baseDir, fanNumber);
						fanMap.put(fc.label().toLowerCase(), fc);
					} catch (IOException e) {
						e.printStackTrace(System.out);
					}
				}

				return false;
			}
		});

		return fanMap;
	}

	@Override
	public int fanNumber() {
		return fanNumber;
	}

	@Override
	public File labelFile() {
		return new File(baseDir, generateFanLabelFileName(fanNumber));
	}

	@Override
	public void setOutputSpeed(int newFanSpeed) throws IOException {
		if (newFanSpeed < minSpeed || newFanSpeed > maxSpeed) {
			throw new IllegalArgumentException("Speed range out of bounds");
		}

		writeFileText(new File(baseDir, generateFanOutputFileName(fanNumber)), String.valueOf(newFanSpeed));
	}

	@Override
	public String toString() {
		return "FileFanControlImpl{" +
				"fanNumber=" + fanNumber +
				", label='" + label + '\'' +
				", maxSpeed=" + maxSpeed +
				", minSpeed=" + minSpeed +
				'}';
	}
}